<script id="module-name-dmsmenusort">
    (function($){
        jQuery(document).ready(function(){
            var $selectors = jQuery('{$dmsmenusortvalue.dms_menus_selector}');
            if( $selectors.length )
            {
                $none_block = '<li class="contain-none-block"><span style="visibility:hidden;">none</span></li>';
                $selectors.each(function(){
                    var $menu_container = jQuery(this);
                    var $listitems = $menu_container.children('li').get();                    
                    if( $listitems.length )
                    {
                        // Alphabet
                        var alphabet = "*!@_.()#^&%-=+01234567989AaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpQqRrSsŚśTtUuVvWwXxYyZzŹźŻż";
                        /*Restract menu poins pozition - Sort menu points*/
                        $listitems.sort(function(a, b) {
                            var compA_container = jQuery(a).clone();
                            compA_container.children('ul').remove();                                    
                            var compA = compA_container.text().toUpperCase();
                            var compB_container = jQuery(b).clone();
                            compB_container.children('ul').remove();
                            var compB = compB_container.text().toUpperCase();
                            var index_a = alphabet.indexOf(compA[0]), index_b = alphabet.indexOf(compB[0]);
                            if( compB == "" )
                            {
                                return -1;
                            }
                            if( compA == "" ) 
                            {
                                return 1;
                            }
                            if(index_a === index_b){
                                if(compA < compB){
                                    return -1;
                                } else if (a > b){
                                    return 1;
                                }
                            } else {
                                return index_a - index_b;
                            }
                            return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
                        })
                        for( $i=0; $i<$listitems.length ; $i++)
                        {
                            $menu_container.append( $listitems[ $i ] );
                        }
                        /*Select menus points by columns*/
                        $menu_list = $menu_container.children('li');
                        if ( $menu_container.css('display') == 'none') {
                            $menu_container.css('display', 'block');
                        }
                        $container_width = $menu_container.width();
                        $point_width = $menu_list.eq(0).width();
                        $menu_container.css('display', 'none');
                        $count_of_columns = Math.floor( $container_width/$point_width );
                        $menu_new = new Array();
                        $element_in_column = Math.ceil( $menu_list.length/$count_of_columns );
                        for( $i=0; $i<$count_of_columns ; $i++)
                        {
                            $column = new Array();                            
                            $start = ($i*$element_in_column);
                            $stop = (( $i+1 ) * $element_in_column );                            
                            for( $j= $start ;(($j<$stop)&&($j<$menu_list.length));$j++  )
                            {
                                $column.push( $menu_list.eq($j) );
                            }
                            $menu_new.push( $column );
                        }
                        /*Restract menu poins pozition - Set columns of menu points in menu*/
                        $category_thumbnail = '';
                        for( $i=0; $i<$element_in_column ; $i++)
                        {
                            for( $j=0;$j<$count_of_columns;$j++  )   
                            {
                                if( typeof $menu_new[ $j ][ $i ] === "undefined" ) 
                                {
                                    $menu_container.append( $none_block );                                    
                                }
                                else if( $menu_new[ $j ][ $i ].hasClass('category-thumbnail') )
                                {
                                    $menu_container.append( $none_block );
                                    $category_thumbnail = $menu_new[ $j ][ $i ];
                                }
                                else
                                {
                                    $menu_container.append( $menu_new[ $j ][ $i ] );
                                }
                            }                            
                        }
                        $menu_container.append( $category_thumbnail );
                    }
                    $menu_container.css('display', 'none');
                });
            }            
        });
    })(jQuery)
</script>