<div class="panel">
    <div class="row panel-heading">
        <h2>{l s='Sort menu by colums' mod='dmsmenusort'}</h2>
    </div>
    <form method="POST" id="dms-menusort-subscribe-form" name="mc-embedded-subscribe-form" class="validate defaultForm form-horizontal" target="_blank" novalidate="">
        <div class="form-wrapper">
            <div class="row">
                <div class="form-group">
                    <label class="control-label col-lg-5">
                           {l s='Menus selectors' mod='dmsmenusort'}
                    </label>
                    <div class="col-lg-6 ">
                        <div class="input-group" style="width: 40%;">
                            <input type="text" name="dms_menus_selector" value="{$fields_value.dms_menus_selector}" placeholder="{l s='Menus selectors' mod='dmsmenusort'}">
                        </div>
                    </div>
                </div>                                
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" name="submitDmsmenusortModule" id="mc-embedded-subscribe" class="btn btn-default pull-right">
                    <i class="process-icon-save"></i>
                    Subscribe
            </button>
        </div>
    </form>
</div>
